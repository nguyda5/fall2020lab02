//David Nguyen, 1938087

package lab02;

public class BikeStore {

	public static void main(String[] args) {
		
		Bicycle[] bicycleItems = new Bicycle[4];
		bicycleItems[0] = new Bicycle("Toyota", 21, 60);
		bicycleItems[1] = new Bicycle("Honda", 11, 75);
		bicycleItems[2] = new Bicycle("Volkswagen", 30, 80);
		bicycleItems[3] = new Bicycle("Mercedes", 10, 50);
		
		for(int i = 0; i < 4; i++) {
			System.out.println(bicycleItems[i].toString());
		}
	}

}
