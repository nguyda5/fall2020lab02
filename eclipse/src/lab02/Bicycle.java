//David Nguyen, 1938087

package lab02;

public class Bicycle {
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;
	
	public Bicycle() {
		manufacturer = "";
		numberGears = 0;
		maxSpeed = 0;
	}
	
	public Bicycle(String userManufacturer, int userNumberGears, int userMaxSpeed) {
		manufacturer = userManufacturer;
		numberGears = userNumberGears;
		maxSpeed = userMaxSpeed;
	}
	
	public String getManufacturer() {
		return this.manufacturer;
	}
	public int getNumberGears() {
		return this.numberGears;
	}
	public double getMaxSpeed() {
		return this.maxSpeed;
	}
	
	public String toString() {
		return ("Manufacturer: " + manufacturer +  " Number of Gears: " + numberGears + " Max Speed: " + maxSpeed);
	}
}
